class Train:
    def __init__(self, train_no, train_name, source, destination):
        self.train_no = train_no
        self.train_name = train_name
        self.source = source
        self.destination = destination
        self.coaches = []

    class Coach:
        def __init__(self, coach_no, coach_type, air_conditioned):
            super().__init__()
            self.coach_no = coach_no
            self.coach_type = coach_type
            if coach_type == 'sleeper':
                self.available_seats = 60
            elif coach_type == 'seater':
                self.available_seats = 120
            self.air_conditioned = air_conditioned
            self.seats = [i for i in range(1, self.available_seats+1)]
            self.booked_seats = []

        def __repr__(self):
            return f'{self.coach_no}-{self.coach_type}-{self.air_conditioned}'

    def add_coach(self, coach):
        """
        This method will add coach to the train
        :param coach:
        :return: None
        """
        for c in self.coaches:
            if c.coach_no == coach.coach_no:
                print('Coach already exists try different coach No.')
                return False
        self.coaches.append(coach)

    def remove_coach(self, coach_no):
        """
        This method will remove coach from the train    
        :param coach_no:
        :return: None
        """
        for coach in self.coaches:
            if coach.coach_no == coach_no:
                self.coaches.remove(coach)

    def show_coaches(self):
        """
        This method will show all coaches of the train
        :return: all coaches
        """
        all_coaches = [coach for coach in self.coaches]
        return all_coaches

    def show_seats(self, coach_no=None):
        """
        This method will show all seats of the train
        :param coach_no:
        :return: all seats
        """
        seats = {'available_seat': {}, 'booked_seat': {}}
        # if coach_no is not None:
        if coach_no:
            for coach in self.coaches:
                if coach.coach_no == str(coach_no):
                    seats["available_seat"][coach.coach_no] = coach.seats
                    seats["booked_seat"][coach.coach_no] = coach.booked_seats
                    return seats
        # if coach_no is None:
        else:
            for coach in self.coaches:
                seats["available_seat"][coach.coach_no] = coach.seats
                seats["booked_seat"][coach.coach_no] = coach.booked_seats
            return seats

    def update_coach(self, coach_no, **kwargs):
        """
        This method will update coach details
        :param coach_no: coach number
        :param kwargs: coach_type, air_conditioned
        :return: None
        """
        for coach in self.coaches:
            if coach.coach_no == coach_no:
                for key, value in kwargs.items():
                    setattr(coach, key, value)

        return "something went wrong while updating"

    def book_seat_of_choice(self, coach_no, *seat_no):
        """
        This method will book seat of choice
        :param coach_no: coach number
        :param seat_no: seat number
        :return: None
        """
        seat_not_available = []
        for coach in self.coaches:
            if coach.coach_no == coach_no:
                for s in seat_no:
                    if s in coach.seats:
                        coach.seats.remove(s)
                        coach.booked_seats.append(s)
                        coach.available_seats -= 1
                    else:
                        seat_not_available.append(s)
                if len(seat_not_available) >= 1:
                    return f'Seat {seat_not_available} is not available'
                else:
                    return 'your all given seats booked successfully'

    def book_any_seat(self, coach_type, air_conditioned):
        """
        This method will book any seat from available seats
        :param coach_type: coach type
        :return: None
        """
        for coach in self.coaches:
            if coach.coach_type == coach_type and air_conditioned == coach.air_conditioned:
                if coach.available_seats > 0:
                    seat = coach.seats.pop()
                    coach.booked_seats.append(seat)
                    coach.available_seats -= 1
                    return f'coach No. {coach.coach_no} seat No. {seat} is booked'
        return 'No seat available in selected type'

    def fetch_all_available_seats(self):
        """
        This method will fetch all available seats
        :return: all available seats"""
        seats = {}
        for coach in self.coaches:
            seats[coach.coach_no] = coach.available_seats
        return seats

    def fetch_all_booked_seats(self):
        """
        This method will fetch all booked seats
        :return: all booked seats  
        """
        seats = {}
        for coach in self.coaches:
            seats[coach.coach_no] = coach.booked_seats
        return seats

    def __repr__(self):
        return f'{self.train_no} {self.train_name} ' \
               f'{self.source} {self.destination} '
