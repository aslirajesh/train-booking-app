
from distutils.log import FATAL
from pickle import FALSE
from train import train

# create a train train
chennai_express = train.Train('1234', 'Chennai Express', 'Chennai',
                              'Bangalore')
# coach
coach1 = chennai_express.Coach('SL1', 'sleeper', "AC")
coach2 = chennai_express.Coach('SL2', 'sleeper', "Non-AC")
coach3 = chennai_express.Coach('SL3', 'sleeper', "Non-AC")
coach4 = chennai_express.Coach('CH1', 'seater',  "Non-AC")
coach5 = chennai_express.Coach('CH2', 'seater', "Non-AC")
coach6 = chennai_express.Coach('CH3', 'seater', "Non-AC")

# add this coach to the train
print("Train")
for i in [coach1, coach2, coach3, coach4, coach5, coach6]:
    chennai_express.add_coach(i)

# print all coaches
print(chennai_express.show_coaches(), '\n')

# adding coach to the train
print("Adding additional coach ch4 to train")
coach7 = chennai_express.Coach('CH4', 'seater', "AC")
chennai_express.add_coach(coach7)
print(chennai_express.show_coaches(), "\n")

# remove coach
print("Removing coach ch4 from train")
chennai_express.remove_coach("CH4")
print(chennai_express.show_coaches(), "\n")

# update coach
print("Updating coach SL2 from non-ac to ac")
chennai_express.update_coach("SL2", air_conditioned="AC")
print(chennai_express.show_coaches(), "\n")

# view all seats in one coach
print("View all seats in one coach")
print(chennai_express.show_seats("SL1"), "\n")

# view all seats in all coaches
print("View all seats in all coaches")
print(chennai_express.show_seats(), "\n")

# random book a seat in seater/sleeper coach
print("Random book a seat in sleeper coach")
print(chennai_express.book_any_seat("sleeper", "Non-AC"), "\n")


# book multiple seats in one coach
print("Book multiple seats in one coach")
print(chennai_express.book_seat_of_choice("SL1", 23, 24, 25, 26, 27), "\n")


# Show all booked seats of a coach
print("Show all booked seats of a coach ex: SL1")
print(chennai_express.show_seats("SL1")["booked_seat"], "\n")

# tryin to book a seat which is already booked
print("Trying to book a seat which is already booked")
print(chennai_express.book_seat_of_choice("SL1", 23), "\n")

# show all booked seats of all coaches
print("API : Show all booked seats of all coaches")
print(chennai_express.fetch_all_booked_seats(), "\n")

# show all available seats of all coaches
print("API: Show all available seats of all coaches")
print(chennai_express.fetch_all_available_seats(), "\n")
