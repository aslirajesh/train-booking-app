# Train booking app




## Train
[SL1-sleeper-AC, SL2-sleeper-Non-AC, SL3-sleeper-Non-AC, CH1-seater-Non-AC, CH2-seater-Non-AC, CH3-seater-Non-AC] 

***Adding additional coach ch4 to train***
```
coach7 = chennai_express.Coach('CH4', 'seater', "AC")
chennai_express.add_coach(coach7)
print(chennai_express.show_coaches())
```
##### [SL1-sleeper-AC, SL2-sleeper-Non-AC, SL3-sleeper-Non-AC, CH1-seater-Non-AC, CH2-seater-Non-AC, CH3-seater-Non-AC, CH4-seater-AC] 

***Removing coach ch4 from train***

```
chennai_express.remove_coach("CH4")
print(chennai_express.show_coaches())
```

##### [SL1-sleeper-AC, SL2-sleeper-Non-AC, SL3-sleeper-Non-AC, CH1-seater-Non-AC, CH2-seater-Non-AC, CH3-seater-Non-AC] 

***Updating coach SL2 from non-ac to ac***

```
chennai_express.update_coach("SL2", air_conditioned="AC")
print(chennai_express.show_coaches())
```

##### [SL1-sleeper-AC, SL2-sleeper-AC, SL3-sleeper-Non-AC, CH1-seater-Non-AC, CH2-seater-Non-AC, CH3-seater-Non-AC] 

***View all seats in one particular coach***

```
print(chennai_express.show_seats("SL1"))
```

##### {'available_seat': {'SL1': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,....., 53, 54, 55, 56, 57, 58, 59, 60]}, 'booked_seat': {'SL1': []}} 

***View all seats in all coaches***

```
print(chennai_express.show_seats())
```

##### {'available_seat': {'SL1': [1, 2, 3, 4, 5, 6, 7,..... 56, 57, 58, 59, 60], 'SL2': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,..... 56, 57, 58, 59, 60], 'SL3': [1, 2, 3, 4, ..... 57, 58, 59, 60], 'CH1': [1, 2, 3, 4, .... 119, 120], 'CH2': [1, 2, 3, 4, 5, 6,.... 119, 120], 'CH3': [1, 2, 3, 4, 5, .... 119, 120]}, 'booked_seat': {'SL1': [], 'SL2': [], 'SL3': [], 'CH1': [], 'CH2': [], 'CH3': []}} 

***Random book a seat in in your choice type sleeper or seater coach***

```
print(chennai_express.book_any_seat("sleeper", "Non-AC"))
```

##### coach No. SL3 seat No. 60 is booked 

***Book multiple seats of your choice in one coach***

```
print(chennai_express.book_seat_of_choice("SL1", 23, 24, 25, 26, 27))
```

##### your all given seats booked successfully 

***Show all booked seats of a coach ex: SL1***

```
print(chennai_express.show_seats("SL1")["booked_seat"])
```

##### {'SL1': [23, 24, 25, 26, 27]} 

***Trying to book a seat which is already booked***

```
print(chennai_express.book_seat_of_choice("SL1", 23))
```

##### Seat [23] is not available 

***API : Show all booked seats of all coaches***

```
print(chennai_express.fetch_all_booked_seats(), "\n")
```


##### {'SL1': [23, 24, 25, 26, 27], 'SL2': [], 'SL3': [60], 'CH1': [], 'CH2': [], 'CH3': []} 

***API: Show all available seats of all coaches***

```
print(chennai_express.fetch_all_available_seats(), "\n")
```

##### {'SL1': 55, 'SL2': 60, 'SL3': 59, 'CH1': 120, 'CH2': 120, 'CH3': 120} 
