class User:
    def __init__(self, name, email, username, password):
        self.name = name
        self.email = email
        self.username = username
        self.password = password
        self.is_admin = False

    def make_admin(self):
        self.is_admin = True

    def remove_admin(self):
        self.is_admin = False

    def __str__(self):
        return f'{self.name} {self.email}'
